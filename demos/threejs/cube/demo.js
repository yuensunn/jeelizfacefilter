"use strict";

// SETTINGS of this demo :
const SETTINGS = {
    rotationOffsetX: 0, // negative -> look upper. in radians
    cameraFOV: 45,      // in degrees, 3D camera FOV
    pivotOffsetYZ: [0, 0, 0], // XYZ of the distance between the center of the cube and the pivot
    detectionThreshold: 0.75, // sensibility, between 0 and 1. Less -> more sensitive
    detectionHysteresis: 0.05,
    scale: 1// scale of the 3D cube
};


// some globalz :
let THREEVIDEOTEXTURE
let THREERENDERER
let THREEFACEOBJ3D
let THREEFACEOBJ3DPIVOTED
let THREESCENE
let THREESCENE_CAMONLY
let THREECAMERA
let ISDETECTED = false;
let action
var modelpath = '../models/Samba Dancing.fbx';
var modelpath1= '../models/emoji/emoji_new13.fbx';
var modelpath2= '../models/emoji/waving_emoji_10.fbx';
var modelpath3= '../models/emoji/Nomnom_20.fbx';
var modelpath4 ='../models/emoji/laughcry_8.fbx'
var modelpath5 ='../models/emoji/lovely_1.fbx'
var modelpath6 ='../models/emoji/politesmile_1.fbx'
var modelpath7 ='../models/emoji/smile_1.fbx'
var modelpath8 ='../models/emoji/sunglasses_1.fbx'
var modelpath9 ='../models/emoji/wink_1.fbx'
var acc1 ='../models/emoji/funnymask_1.fbx'
var acc2 ='../models/emoji/freckles_1.fbx'
var mixers = [];
var clock = new THREE.Clock();


var modelmodel = modelpath4;


var elapsedTime = 0;

// callback : launched if a face is detected or lost. TODO : add a cool particle effect WoW !
function detect_callback(isDetected) {
    if (isDetected) {
        console.log('INFO in detect_callback() : DETECTED');
    } else {
        console.log('INFO in detect_callback() : LOST');
    }
}

// build the 3D. called once when Jeeliz Face Filter is OK
function init_threeScene(spec) {

    //  var THREE = require('three');

    // INIT THE THREE.JS context
    THREERENDERER = new THREE.WebGLRenderer({
        //antialias: true,
        context: spec.GL,
        canvas: spec.canvasElement
    });

    // COMPOSITE OBJECT WHICH WILL FOLLOW THE HEAD
    // in fact we create 2 objects to be able to shift the pivot point


    THREEFACEOBJ3D = new THREE.Object3D();
    THREEFACEOBJ3D.frustumCulled = false;
    THREEFACEOBJ3DPIVOTED = new THREE.Object3D();
    THREEFACEOBJ3DPIVOTED.frustumCulled = false;
    THREEFACEOBJ3DPIVOTED.position.set(SETTINGS.pivotOffsetYZ[0], SETTINGS.pivotOffsetYZ[1], SETTINGS.pivotOffsetYZ[2]);
    THREEFACEOBJ3DPIVOTED.scale.set(SETTINGS.scale, SETTINGS.scale, SETTINGS.scale);

    THREEFACEOBJ3D.add(THREEFACEOBJ3DPIVOTED);
    THREEFACEOBJ3DPIVOTED.translateY(-0.35);
    THREEFACEOBJ3DPIVOTED.translateZ(0.3);
    THREEFACEOBJ3D.position.set(0,0,0);
    
    $.getJSON( "./data.json", function( data ) {
        $.each( data, function( key, val ) {
            loadFBX(val);
        });
    });
 
    
    // var loader = new THREE.FBXLoader();


    // loader.load( modelmodel, function ( object ) {
    //     object.mixer = new THREE.AnimationMixer( object );
    //     mixers.push( object.mixer );

    //     action = object.mixer.clipAction( object.animations[ 0 ] );
    //     action.play();
    //     console.log(object);
    //     THREEFACEOBJ3DPIVOTED.add( object );        

    //     THREEFACEOBJ3DPIVOTED.traverse(function (object) {
    //         console.log(object.name);
    //     })
    
        
    // } );



    // CREATE THE SCENE
    THREESCENE = new THREE.Scene();
    THREESCENE_CAMONLY = new THREE.Scene();
    THREESCENE.add(THREEFACEOBJ3D);

    // init video texture with red
    THREEVIDEOTEXTURE = new THREE.DataTexture(new Uint8Array([255,255,255]), 1, 1, THREE.RGBFormat);
    THREEVIDEOTEXTURE.needsUpdate = false;

    // CREATE THE VIDEO BACKGROUND
    const videoMaterial = new THREE.RawShaderMaterial({
        depthWrite: false,
        depthTest: false,
        vertexShader: "attribute vec2 position;\n\
            varying vec2 vUV;\n\
            void main(void){\n\
                gl_Position=vec4(position, 0., 1.);\n\
                vUV=0.5+0.5*position;\n\
            }",
        fragmentShader: "precision lowp float;\n\
            uniform sampler2D samplerVideo;\n\
            varying vec2 vUV;\n\
            void main(void){\n\
                gl_FragColor=texture2D(samplerVideo, vUV);\n\
            }",
         uniforms:{
            samplerVideo: { value: THREEVIDEOTEXTURE }
         }
    });
    const videoGeometry = new THREE.BufferGeometry()
    const videoScreenCorners = new Float32Array([-1,-1,   1,-1,   1,1,   -1,1]);
    videoGeometry.addAttribute('position', new THREE.BufferAttribute( videoScreenCorners, 2));
    videoGeometry.setIndex(new THREE.BufferAttribute(new Uint16Array([0,1,2, 0,2,3]), 1));
    const videoMesh = new THREE.Mesh(videoGeometry, videoMaterial);
    videoMesh.onAfterRender = function () {
        // replace THREEVIDEOTEXTURE.__webglTexture by the real video texture
        THREERENDERER.properties.update(THREEVIDEOTEXTURE, '__webglTexture', spec.videoTexture);
        THREEVIDEOTEXTURE.magFilter = THREE.LinearFilter;
        THREEVIDEOTEXTURE.minFilter = THREE.LinearFilter;
        delete(videoMesh.onAfterRender);
    };
    videoMesh.renderOrder = -1000; // render first
    videoMesh.frustumCulled = false;
    THREESCENE.add(videoMesh);
    //THREESCENE_CAMONLY.add(videoMesh);
    // CREATE THE CAMERA
    const aspecRatio = spec.canvasElement.width / spec.canvasElement.height;
    THREECAMERA = new THREE.PerspectiveCamera(SETTINGS.cameraFOV, aspecRatio, 2, 1000);

    console.log("position "+THREECAMERA.getWorldPosition());

    const ambient = new THREE.AmbientLight(0xfffffa, 2);
    THREESCENE.add(ambient)
    
    // CREATE A SPOTLIGHT
    //var spotLight = new THREE.SpotLight(0xffffff);
    //spotLight.position.set(100, 1000, 100);

    //spotLight.castShadow = true;
    //THREESCENE.add(spotLight)
} // end init_threeScene()




function mapAccesory(emoji,accessory,target){


    
}

function loadFBX(models){

    console.log(models);
  // var THREE = require("three");
    var index = 0;
    var objLoader = new THREE.FBXLoader();
    loadNextFile();
    function loadNextFile() {
        
        if (index > models.length - 1){
          
            return;
        }
        objLoader.load(models[index].src, function(loaded) {

            console.log(loaded);
            if(models[index].type =="emoji"){
                loaded.mixer = new THREE.AnimationMixer( loaded );
                mixers.push( loaded.mixer );
                action = loaded.mixer.clipAction( loaded.animations[ 0 ] );
                action.play();
        


                THREEFACEOBJ3DPIVOTED.add( loaded ); 
                loaded.traverse(x=>{console.log(x.name);});
            }else{
                 models[0].object.getObjectByName(models[index].anchor).add(loaded);

            }
            console.log("index "+index);
            loaded.position.set(0,0,0);
            models[index].object = loaded;
            index++;
            loadNextFile();
        });

    }



    console.log(models.filter(x=>x.type =="emoji").length);

}




//launched by body.onload() :
function main(){
    JeelizResizer.size_canvas({
        canvasId: 'jeeFaceFilterCanvas',
        callback: function(isError, bestVideoSettings){
            init_faceFilter(bestVideoSettings);
        }
    })
} //end main()



function init_faceFilter(videoSettings){


    var some = JEEFACEFILTERAPI.init({
        canvasId: 'jeeFaceFilterCanvas',
        NNCpath: '../../../dist/', // root of NNC.json file
        videoSettings: {
            maxWidth: 320,
            maxHeight: 480
        },
        callbackReady: function (errCode, spec) {
            if (errCode) {
                console.log('AN ERROR HAPPENS. SORRY BRO :( . ERR =', errCode);
                return;
            }

            console.log('INFO : JEEFACEFILTERAPI IS READY');
            init_threeScene(spec);
            
        }, // end callbackReady()

        // called at each render iteration (drawing loop)
        callbackTrack: function (detectState) {
   
            if (ISDETECTED && detectState.detected < SETTINGS.detectionThreshold - SETTINGS.detectionHysteresis) {
                // DETECTION LOST
                THREEFACEOBJ3D.position.set(-100, -100, -100);
                detect_callback(false);
                ISDETECTED = false;
            } else if (!ISDETECTED && detectState.detected > SETTINGS.detectionThreshold + SETTINGS.detectionHysteresis) {
                // FACE DETECTED
                detect_callback(true);
                ISDETECTED = true;
            }
            //THREERENDERER.render(THREESCENE_CAMONLY, THREECAMERA);
            if (ISDETECTED) {
                // move the cube in order to fit the head
                const tanFOV = Math.tan(THREECAMERA.aspect * THREECAMERA.fov * Math.PI / 360); // tan(FOV/2), in radians
                const W = detectState.s;  // relative width of the detection window (1-> whole width of the detection window)
                const D = 1 / (2 * W * tanFOV); // distance between the front face of the cube and the camera
                
                // coords in 2D of the center of the detection window in the viewport :
                const xv = detectState.x;
                const yv = detectState.y;
                
                // coords in 3D of the center of the cube (in the view coordinates system)
                const z = -D - 0.5;   // minus because view coordinate system Z goes backward. -0.5 because z is the coord of the center of the cube (not the front face)
                const x = xv * D * tanFOV;
                const y = yv * D * tanFOV / THREECAMERA.aspect;
             
                // move and rotate the cube
               THREEFACEOBJ3D.position.set(x, y + SETTINGS.pivotOffsetYZ[0], z + SETTINGS.pivotOffsetYZ[1]);
               THREEFACEOBJ3D.rotation.set(detectState.rx + SETTINGS.rotationOffsetX, detectState.ry, detectState.rz, "XYZ");
            
       
            }


        


    
           // console.log(elapsedTime); 
          
        
     
        THREERENDERER.state.reset();
        animate();
           // THREERENDERER.state.reset();
        //    animate();
        //    // console.log(elapsedTime); 
        //     animate();
            //THREERENDERER.render(THREESCENE, THREECAMERA);
            // reinitialize the state of THREE.JS because JEEFACEFILTER have changed stuffs
  

            // trigger the render of the THREE.JS SCENE
        } // end callbackTrack()
    }); // end JEEFACEFILTERAPI.init call

} // end main()

function animate() {
    //requestAnimationFrame( animate );
    if ( mixers.length > 0 ) {
        for ( var i = 0; i < mixers.length; i ++ ) {
            mixers[ i ].update( clock.getDelta() );
        }
    }
    THREERENDERER.render(THREESCENE, THREECAMERA);
}

