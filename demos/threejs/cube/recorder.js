window.AudioContext = window.AudioContext || window.webkitAudioContext;
        var audioContext = new AudioContext();
        document.getElementById('btn-record-webm').onclick = function() {
            this.disabled = true;

            navigator.mediaDevices.getUserMedia({audio: true}).then(function(audioStream) {
                
                var canvas = document.getElementById('jeeFaceFilterCanvas');
                var canvasStream = canvas.captureStream();

                //var finalStream = new MediaStream();
                //audioStream.getAudioTracks().forEach(function(track) {
                //    finalStream.addTrack(track);
                //});
                
                var finalStream = gotStream(audioStream);
                canvasStream.getVideoTracks().forEach(function(track) {
                    finalStream.addTrack(track);
                });

                var recorder = RecordRTC(finalStream, {
                    type: 'video',
                    mimeType: 'video/webm\;codecs=h264'
                });

                recorder.startRecording();

                var stop = false;
                
                (function looper() {
                    if(stop) {
                        recorder.stopRecording(function() {
                            var blob = recorder.getBlob();
                            //document.body.innerHTML = '<video controls src="' + URL.createObjectURL(blob) + '" autoplay loop></video>';
                            recorder.save('test.mp4');
                            recorder.writeToDisk();
                            audioStream.stop();
                            canvasStream.stop();
                        });
                        return;
                    }
                    setTimeout(looper, 1000/60);
                })();

                var seconds = 10;
                var interval = setInterval(function() {
                    seconds--;
                    if(document.querySelector('h1')) {
                        document.querySelector('h1').innerHTML = seconds + ' seconds remaining.';
                    }
                }, 1000);

                setTimeout(function() {
                    clearTimeout(interval);
                    stop = true;
                }, seconds * 1000);
            });
        };

        function gotStream(audioStream) {
            // Create an AudioNode from the stream.
            //    realAudioInput = audioContext.createMediaStreamSource(stream);
            var audioInput = audioContext.createMediaStreamSource(audioStream);
            var audioOutput = audioContext.createMediaStreamDestination();
            /*
                realAudioInput = audioContext.createBiquadFilter();
                realAudioInput.frequency.value = 60.0;
                realAudioInput.type = realAudioInput.NOTCH;
                realAudioInput.Q = 10.0;

                input.connect( realAudioInput );
            */
            //audioInput = convertToMono( input );

            // create mix gain nodes
            outputMix = audioContext.createGain();
            dryGain = audioContext.createGain();
            wetGain = audioContext.createGain();
            //effectInput = audioContext.createGain();
            effectInput = createPitchShifter();
            audioInput.connect(dryGain);
            audioInput.connect(effectInput);
            dryGain.connect(outputMix)
            wetGain.connect(outputMix);
            //outputMix.connect( audioContext.destination);
            //dryGain.gain.value = 1.0;
            //wetGain.gain.value = 1.0;
            crossfade(1.0);
            outputMix.connect(audioOutput);
            
            return audioOutput.stream;
        }

        function convertToMono( input ) {
            var splitter = audioContext.createChannelSplitter(2);
            var merger = audioContext.createChannelMerger(2);

            input.connect( splitter );
            splitter.connect( merger, 0, 0 );
            splitter.connect( merger, 0, 1 );
            return merger;
        }

        function crossfade(value) {
            // equal-power crossfade
            var gain1 = Math.cos(value * 0.5*Math.PI);
            var gain2 = Math.cos((1.0-value) * 0.5*Math.PI);

            dryGain.gain.value = gain1;
            wetGain.gain.value = gain2;
            }

        function createPitchShifter() {
            effect = new Jungle( audioContext );
            effect.setPitchOffset(1.0);
            effect.output.connect( wetGain );
            return effect.input;
        }